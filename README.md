# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

1. Download and Install IAR Embedded Workbench 7.4 (https://www.iar.com/iar-embedded-workbench/partners/texas-instruments/ti-wireless/).
2. Download and Install ZStack Home Automation 1.2.2 (http://www.ti.com/tool/z-stack-archive).
3. Download and Install CADpass (http://www.cmc.ca/en/WhatWeOffer/Design/Tools/CADPass.aspx).
4. Create CMC student account and link with Dr. Meng to gain access to IAR license.
5. Download Repository into ti\simplelink\zstack_home_1_02_02a_44539\Projects\zstack\HomeAutomation\ (This file location will be where ever the ZStack was installed).

* Configuration

1. Once setup is finished open the IAR project called SensorTag.
2. Go to "Tools > Configure Custom Argument Variables".
3. Delete the current "CC26xx TI-RTOS" variables.
4. Import "SensorTag.custom_argvars" (This file will be located inside the CC2650 folder in the project).
5. Confirm/Edit the location of the Custom Argument Variables.
6. Once variables are pointing to the proper location press "Ok".
7. Make the project to confirm environment is setup.

* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact